package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author ramses trejo - 23046
 *
 */

public class PasswordValidatorTest {
	
	
	@Test
	public void testHasValidCaseCharsRegular(  ) {
		assertTrue( "Invalid case chars" ,  PasswordValidator.hasValidCaseChars( "fdfdRfff" ) );
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn(  ) {
		assertTrue( "Invalid case chars" ,  PasswordValidator.hasValidCaseChars( "Aa" ) );
	}	
	
	@Test
	public void testHasValidCaseCharsException(  ) {
		assertFalse( "Invalid case chars"  , PasswordValidator.hasValidCaseChars( "12345" ) );
	}	
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty(  ) {
		assertFalse( "Invalid case chars"  , PasswordValidator.hasValidCaseChars( "" ) );
	}		
	
	@Test
	public void testHasValidCaseCharsExceptionNull(  ) {
		assertFalse( "Invalid case chars"  , PasswordValidator.hasValidCaseChars( null ) );
	}	
	
	@Test
	public void testHasValidCaseCharsExceptionBoundaryOutUpper(  ) {
		assertFalse( "Invalid case chars"  , PasswordValidator.hasValidCaseChars( "A" ) );
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBoundaryOutLower(  ) {
		assertFalse( "Invalid case chars"  , PasswordValidator.hasValidCaseChars( "a" ) );
	}		
	
	
	///////////
	
	@Test 
	public void testIsValidLengthRegular( ) {
		boolean result = PasswordValidator.isValidLength( "123456789012" );
		assertTrue(  "Invalid length" , result );
	}
	
	@Test 
	public void testIsValidLengthBoundaryIn( ) {
		boolean result = PasswordValidator.isValidLength( "12345678" );
		assertTrue( "Invalid length"  , result );
	}
		
	
	@Test 
	public void testIsValidLengthException( ) {
		boolean result = PasswordValidator.isValidLength( "" );
		assertFalse( "Invalid length"  , result );
	}
	
	@Test 
	public void testIsValidLengthExceptionSpaces( ) {	
		boolean result = PasswordValidator.isValidLength( "    t  e  s  t       " );
		assertFalse( "Invalid length"  , result );
	}
	
	@Test 
	public void testIsValidLengthBoundaryOut( ) {
		boolean result = PasswordValidator.isValidLength( "1234567" );
		assertFalse( "Invalid length"  , result );
	}	
	
	@Test 
	public void testHasValidDigitCountRegular( ) {
		boolean result = PasswordValidator.hasValidDigitCount("ss4sd8s4ds");
		assertTrue( "Invalid number of digits" , result );
	}
	
	@Test 
	public void testHasValidDigitCountBoundaryIn( ) {
		boolean result = PasswordValidator.hasValidDigitCount("kdk5jhjd4k8fjjjhjj");
		assertTrue( "Invalid number of digits" , result );
	}	
	
	@Test 
	public void testHasValidDigitCountException( ) {
		boolean result = PasswordValidator.hasValidDigitCount("kdkdkdkfj");
		assertFalse( "Invalid number of digits" , result );
	}	
	
	@Test 
	public void testHasValidDigitCountBoundaryOut( ) {
		boolean result = PasswordValidator.hasValidDigitCount("kdkd4kdkfj");
		assertFalse( "Invalid number of digits" , result );
	}	
	

}
